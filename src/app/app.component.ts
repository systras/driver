import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Component, ViewChild } from '@angular/core';
import { ProfileProvider } from '../providers/profile/profile';
import { OneSignal} from '@ionic-native/onesignal';
import { EventProvider } from '../providers/event/event';
import { LanguageProvider } from '../providers/language/language';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navct: Nav;
  public user: any;
  rootPage:any = 'HomePage';
  phone: any;
  picture: any;
  active_User: any;
  pages: any;
  showSplash = true;

  constructor( public storage: Storage, public lp: LanguageProvider, private One: OneSignal, public ev:EventProvider, public ph: ProfileProvider, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    this.pages = [
      //Add pages for sidemenu
      {  component: 'HistoryPage', icon: "ios-card" },
      {  component: 'ProfilePage', icon: "ios-book" },
      {  component: 'SupportPage', icon: "ios-chatbubbles" },
      {  component: 'SettingsPage', icon: "ios-settings" },
      {  component: 'SchedulePage', icon: "ios-timer" },
    ];


  }




  initializeApp(){
    //alert('initializedß')
    this.platform.ready().then(() => {

  
    //Similar to the main 
    if (this.platform.is('cordova')) {
     //this.One.setLogLevel({logLevel: 4, visualLevel: 4})
     //Replace with your app id and google cloud messaging id from https://onesignal.com 
      this.One.startInit("f6345b2b-3777-4631-8415-a3a178826e8d", "1003487763108");
      this.One.inFocusDisplaying(this.One.OSInFocusDisplayOption.Notification);
      this.One.setSubscription(true);
      this.One.enableVibrate(true);
      
      this.One.handleNotificationReceived().subscribe((data) => {
        // handle received here how you wish.
       // this.pop.isNewJob = true;
      });
      this.One.handleNotificationOpened().subscribe((data) => {
        // handle opened here how you wish.
      });
      this.One.endInit();  
    
       
    }

    if (this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString("#313131");
      }else{
        this.statusBar.styleDefault();
        //statusBar.backgroundColorByHexString('#000000');
      }

    this.splashScreen.hide();
 
    });
  }

  openPage(page) {
    //open side menu pages on click
    this.platform.registerBackButtonAction(()=>this.navct.popToRoot());
    this.navct.push(page);
  }




  gotoAbout() {
    //open top menu from side bar menu
    this.navct.push('AboutPage');
    this.platform.registerBackButtonAction(()=>this.navct.popToRoot());
  }

  
    
  }
