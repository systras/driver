import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ProfileProvider} from '../../providers/profile/profile';
import {PopUpProvider} from '../../providers/pop-up/pop-up';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-phone',
    templateUrl: 'phone.html',
})
export class PhonePage {

    constructor(
        public navCtrl: NavController,
        public pop: PopUpProvider,
        public ph: ProfileProvider,
        public navParams: NavParams
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
        // timer(500).subscribe(()=>
        this.login()
        //this.pop.showLoader('')
        // )
    }

    login() {
        (<any>window).AccountKitPlugin.loginWithPhoneNumber({
            useAccessToken: true,
            defaultCountryCode: "pt_BR",
            facebookNotificationsEnabled: true
        }, (successdata) => {
            (<any>window).AccountKitPlugin.getAccount((user) => {
                this.pop.showLoader('')
                //alert(user.phoneNumber)

                if (user.phoneNumber)
                    this.ph.UpdateNumber(user.phoneNumber).then(() => {

                        console.log(this.navParams.get("email"));

                        this.navCtrl.setRoot('MoreInfoPage', {
                            email: this.navParams.get("email"),
                            password: this.navParams.get("password"),
                            firebase_id: this.navParams.get('id_firebase'),
                            phone: user.phoneNumber
                        });
                        this.pop.hideLoader()

                    })

            })
        }, (err) => {
            // alert(err);
            this.login()
        })
    }

}