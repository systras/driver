import {Component} from "@angular/core";
import {Platform, NavParams} from "ionic-angular";
import {ProfileProvider} from "../../providers/profile/profile";
import {PopUpProvider} from "../../providers/pop-up/pop-up";
import {SystrasProvider} from "../../providers/systras/systras";

import {
    NavController,
    Loading,
    AlertController,
    MenuController
} from "ionic-angular";

import {IonicPage} from "ionic-angular";
import {LanguageProvider} from "../../providers/language/language";
import {OneSignal} from "@ionic-native/onesignal";

@IonicPage()
/**
 * Generated class for the MoreInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: "page-more-info",
    templateUrl: "more-info.html"
})
export class MoreInfoPage {
    loading: Loading;
    state: any;
    card: any;
    cartypes: any = "6";
    cities: any = "Campinas";
    name: any;
    email: any;
    license: any = "";
    company_code: any = '';
    model: any = "";
    plate: any = "";
    year: any;
    seat: any;
    banks: any;
    agencia: any;
    conta: any;
    number: any;
    phoneNumber: any = "";
    userName: any = "";
    countryCode: any = "";
    result: any;

    constructor(
        public platform: Platform,
        public navParams: NavParams,
        public lp: LanguageProvider,
        public pop: PopUpProvider,
        public navCtrl: NavController,
        public menu: MenuController,
        public ph: ProfileProvider,
        public alertCtrl: AlertController,
        public systras: SystrasProvider,
        public one: OneSignal
    ) {
        // this.phoneNumber = this.navParams.get('phone');
    }

    ionViewDidLoad() {
        //console.log("ionViewDidLoad PhonePage");
        //this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    }

    signIn(userName: string) {
        //Step 2 - Pass the mobile number for verific
        if (
            this.name != null &&
            this.banks != null &&
            this.conta != null &&
            this.cities &&
            this.model &&
            this.year &&
            this.license &&
            this.plate &&
            this.seat
        ) {
            this.number = this.agencia + "," + this.conta;

            this.one.getIds().then(id => {
                this.pop.showLoader("");
                this.ph
                    .UpdateInfo(
                        this.name,
                        this.cities,
                        this.license,
                        this.model,
                        this.year,
                        this.plate,
                        this.seat,
                        this.number,
                        this.banks,
                        this.cartypes
                    )
                    .then(success => {
                        this.systras.setDados(this.name,
                            this.navParams.get('email'),
                            this.navParams.get('firebase_id'),
                            id.userId,
                            this.company_code,
                            this.navParams.get('password'),
                            `${this.banks} ${this.number}`,
                            this.plate,
                            this.license,
                            this.model,
                            this.year,
                            this.seat,
                            this.cartypes,
                            this.navParams.get('phone')
                        ).subscribe(ret => {
                            this.navCtrl.setRoot("AddphotoinfoPage");
                            this.pop.hideLoader();
                        }, error => {
                            this.pop.hideLoader();
                            let alert = this.alertCtrl.create({
                                title: 'Falha ao se comunicar',
                                subTitle: 'Verifique a sua internet',
                                buttons: ['Fechar']
                            });
                            alert.present();
                        });
                    });
            });
        }
    }
}
