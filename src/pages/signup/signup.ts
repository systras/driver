import {Component} from '@angular/core';
import firebase from 'firebase';
import {
    NavController,
    Loading,
    LoadingController,
    AlertController
} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthProvider} from '../../providers/auth/auth';
import {EmailValidator} from '../../validators/email';
import {IonicPage} from 'ionic-angular';
import {ProfileProvider} from '../../providers/profile/profile';
import {LanguageProvider} from '../../providers/language/language';

@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {
    public signupForm: FormGroup;
    loading: Loading;
    public userProfileRef: firebase.database.Reference;

    constructor(
        public navCtrl: NavController,
        public lp: LanguageProvider,
        public authProvider: AuthProvider,
        public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public ph: ProfileProvider,
        public alertCtrl: AlertController
    ) {
        this.userProfileRef = firebase.database().ref('/driverProfile');
        this.signupForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
        });
    }

    signupUser() {
        if (!this.signupForm.valid) {
            console.log(this.signupForm.value);
        } else {
            this.authProvider.signupUser(this.signupForm.value.email, this.signupForm.value.password).then(newUser => {
                this.userProfileRef.child(newUser.uid).set({
                    email: this.signupForm.value.email
                }).then(() => {
                    this.loading.dismiss().then(() => {
                        this.navCtrl.setRoot('PhonePage', {
                            'email': this.signupForm.value.email,
                            'password': this.signupForm.value.password,
                            "id_firebase": newUser.uid
                        });
                    });
                });
                this.loading = this.loadingCtrl.create();
                this.loading.present();
            });
        }
    }


    goToLogin() {
        this.navCtrl.setRoot('LoginPage');
    }


}
