import {Component} from "@angular/core";
import {
    NavController,
    AlertController,
    LoadingController,
    NavParams,
    Events
} from "ionic-angular";
import {EventProvider} from "../../providers/event/event";
import {ProfileProvider} from "../../providers/profile/profile";
import {IonicPage, Platform} from "ionic-angular";
import {PopUpProvider} from "../../providers/pop-up/pop-up";
import {SettingsProvider} from "../../providers/settings/settings";
import {OnesignalProvider} from "../../providers/onesignal/onesignal";
import {LocationTrackerProvider} from "../../providers/location-tracker/location-tracker";
import {GeocoderProvider} from "../../providers/geocoder/geocoder";
import {LanguageProvider} from "../../providers/language/language";
import {OneSignal} from "@ionic-native/onesignal";
import {Geolocation} from "@ionic-native/geolocation";

declare var google;

@IonicPage()
@Component({
    selector: "page-schedule",
    templateUrl: "schedule.html"
})
export class SchedulePage {
    public scheduleList: Array<any>;
    currentYear: any;
    currentMonth: any;
    currentDay: any;
    userPos: any;
    lat: any;
    lng: any;
    notif_id: any;
    locationName: any;
    public geocoder: any = new google.maps.Geocoder();

    constructor(
        public navCtrl: NavController,
        public One: OneSignal,
        public lp: LanguageProvider,
        public events: Events,
        public platform: Platform,
        public navParams: NavParams,
        public gCode: GeocoderProvider,
        public locat: LocationTrackerProvider,
        public settings: SettingsProvider,
        public oneSignal: OnesignalProvider,
        public pop: PopUpProvider,
        public load: LoadingController,
        public profile: ProfileProvider,
        public alert: AlertController,
        public eventProvider: EventProvider,
        private geolocation: Geolocation
    ) {
        this.currentYear = new Date().getFullYear();
        this.currentMonth = new Date().getUTCMonth() + 1;
        this.currentDay = new Date().getUTCDate();

        this.geolocation
            .getCurrentPosition()
            .then(ret => {
                this.lat = ret.coords.latitude;
                this.lng = ret.coords.longitude;
                this.userPos = new google.maps.LatLng(this.lat, this.lng);
                let latlng = {lat: parseFloat(this.lat), lng: parseFloat(this.lng)};
                this.geocoder.geocode({location: latlng}, (results, status) => {
                    if (status === "OK") {
                        this.locationName = results[0].formatted_address;
                        this.findScheduledRides();
                    } else {
                        this.pop.showPimp("Falha ao buscar localização");
                    }
                });
            })
            .catch(error => {
                this.pop.showPimp("Falha ao buscar localização");
            });
    }

    findScheduledRides() {
        this.eventProvider.getScheduleList().on("value", snapshot => {
            this.scheduleList = [];
            snapshot.forEach(snap => {
                snap.forEach(item => {
                    let today = new Date();
                    let future = new Date(item.val().Client_Date);

                    let a = this.calcDate(today, future);

                    let begin = this.userPos;

                    let end = new google.maps.LatLng(
                        item.val().Client_location[0],
                        item.val().Client_location[1]
                    );

                    console.log(a);
                    console.log(this.profile.user.uid);

                    let suc = google.maps.geometry.spherical.computeDistanceBetween(
                        begin,
                        end
                    ) / 1000;

                    if(suc <= 50){
                        this.scheduleList.push({
                            id: item.key,
                            name: item.val().Client_username,
                            price: item.val().Client_price,
                            date: a[0],
                            duedate: a[1],
                            canBook: true,
                            time: item.val().Client_Time,
                            location: item.val().Client_locationName,
                            destination: item.val().Client_destinationName,
                            payWith: item.val().Client_paymentForm,
                            picture: item.val().Client_picture,
                            phone: item.val().Client_phoneNumber,
                            lat: item.val().Client_location[0],
                            lng: item.val().Client_location[1],
                            rating: item.val().Client_Rating,
                            ratingText: item.val().Client_RatingText
                        });
                    }

                    return false;
                });

                return false;
            });
        });

        this.presentRouteLoader("");
    }

    Warn(time) {
        this.pop.showPimp(this.lp.translate()[0].k7);
    }

    calcDate(date1, date2) {
        var diff = Math.floor(date2.getTime() - date1.getTime());
        var day = 1000 * 60 * 60 * 24;

        var days = Math.floor(diff / day);
        var months = Math.floor(days / 31);
        var years = Math.floor(months / 12);

        console.log(days);

        var message = [date2.toDateString(), days];
        // message += " was "
        // message += days + " days "
        // message += months + " months "
        // message += years + " years ago \n"

        return message;
    }

    presentRouteLoader(message) {
        let loading = this.load.create({
            content: message
        });

        loading.present();

        let myInterval = setInterval(() => {
            if (this.scheduleList != null) {
                loading.dismiss();
                clearInterval(myInterval);
            }
        }, 1000);
    }

    Start(e) {
        if (!this.platform.is("cordova")) {
            this.notif_id = "3456789098765456733";
        } else {
            this.One.getIds().then(success => {
                this.notif_id = success.userId;
            });
        }

        if (this.notif_id)
            if (this.notif_id != e.id) {
                if (!this.platform.is("cordova")) {
                    this.locat.lat = -22.886495;
                    this.locat.lng = -47.0432657;
                    this.gCode.locationName = "Campinas, São Paulo";
                    this.locationName = "Campinas, São Paulo";
                    this.lat = -22.886495;
                    this.lng = -47.0432657;
                }

                this.settings.id = e.id;
                console.log(this.settings.id);
                this.settings.current_ID = true;
                console.log(e);
                console.log(this.locat.lat);
                this.locat.hasTransactionEnded = true;
                this.locat.canTrack_ = true;

                console.log("Stopped Normal Movement Of Driver");
                this.oneSignal.isInDeepSh_t = true;

                this.profile
                    .getUserAsClientInfo(this.navParams.get("notif"))
                    .off("child_added");
                this.profile
                    .getUserAsClientInfo(this.navParams.get("notif"))
                    .off("child_changed");
                this.profile
                    .getUserAsClientInfo(this.navParams.get("notif"))
                    .off("child_removed");

                this.eventProvider
                    .getCurrentDriver()
                    .remove()
                    .then(id => {
                        //Push the drivers information to the customer connection node in the database
                        this.eventProvider
                            .getActivityProfile(e.id)
                            .set({
                                Client_username: e.name,
                                Client_Deleted: false,
                                Client_location: [e.lat, e.lng],
                                Client_locationName: e.location,
                                Client_paymentForm: e.payWith,
                                Client_picture: e.picture,
                                Client_phoneNumber: e.phone,
                                Client_destinationName: e.destination,
                                Client_CanChargeCard: false,
                                Client_PickedUp: false,
                                Client_Dropped: false,
                                Client_HasRated: false,
                                Client_ended: false,
                                Client_price: e.price,
                                Client_Rating: e.rating,
                                Client_RatingText: e.ratingText,
                                entered: 0,
                                Driver_name: this.profile.name,
                                Driver_location: [this.lat, this.lng],
                                Driver_locationName: this.locationName,
                                Driver_plate: this.profile.plate,
                                Driver_picture: this.profile.picture,
                                Driver_ID: "43566625",
                                Driver_seats: this.profile.seat,
                                Driver_carType: this.profile.carType,
                                Driver_rating: this.profile.rating,
                                Driver_number: this.profile.number,
                                Driver_HasRated: false
                            })
                            .then(suc => {
                                //After removing the driver from the driver node we can then create a push notification to this driver
                                if (this.platform.is("cordova")) {
                                    this.oneSignal.sendPush(e.id);
                                }

                                this.platform.ready().then(() => {
                                    this.platform.registerBackButtonAction(() =>
                                        this.pop.presentToast(this.lp.translate()[0].k8)
                                    );
                                });

                                this.navCtrl.pop().then(() => {
                                    /// Trigger custom event and pass data to be send back
                                    this.settings.id = e.id;
                                    console.log(this.settings.id);
                                });

                                //  this.navCtrl.pop({verificationid: e.key});
                            });
                    });
            } else {
                this.pop.showPimp("Run On different Devices to test Ride Scheduling");
            }
    }
}
