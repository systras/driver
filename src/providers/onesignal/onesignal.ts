import {Injectable} from '@angular/core';
import {OneSignal} from '@ionic-native/onesignal';
import {EventProvider} from '../../providers/event/event';
import {Platform} from 'ionic-angular';
import {LanguageProvider} from '../../providers/language/language';
import {ProfileProvider} from '../../providers/profile/profile';

/*
  Generated class for the OnesignalProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class OnesignalProvider {
    public notif_id: any;
    public isInDeepSh_t: boolean = true;
    public isInDeepSh_t2: boolean = true;
    public ID: any;
    phone: any;
    picture: any;
    rating: any;

    constructor(
        public One: OneSignal,
        public ph: ProfileProvider,
        public lp: LanguageProvider,
        public eprov: EventProvider,
        public platform: Platform) {

        if (!this.platform.is('cordova')) {
            this.notif_id = "3456787654567898765434567";

        }
        else {
            this.One.getIds().then(success => {
                this.notif_id = success.userId
            })
        }
    }

    ionViewLoaded() {

    }


    sendPush(id) {
        this.One.getIds().then(success => {
            console.log(success.userId);

            let notificationObj: any = {
                include_player_ids: [id],
                contents: {en: this.lp.translate()[0].f6},
            };

            this.One.postNotification(notificationObj).then(success => {
                console.log("Notification Post Success:", success);
            }, error => {
                console.log(error);
                alert("Notification Post Failed:\n" + JSON.stringify(error));
            });

        })
    }


    UpdateInfo(lat, lng, car) {
        this.ph.getUserProfile().on('value', userProfileSnapshot => {
            this.phone = userProfileSnapshot.val().phoneNumber;
            this.picture = userProfileSnapshot.val().picture;
            this.rating = userProfileSnapshot.val().rating;

            if (this.platform.is('cordova')) {
                this.One.getIds().then(success => {
                    this.notif_id = success.userId
                    this.eprov.PushCurrentLocation(lat, lng, success.userId, car, this.phone, this.picture, this.rating)
                })
            } else {
                console.log()
                console.log('updated', lat, lng, this.notif_id, car, this.phone, this.picture, this.rating)
                this.eprov.PushCurrentLocation(lat, lng, this.notif_id, car, this.phone, this.picture, this.rating)
            }

        })
    }

    UpdateCurrentLocation(lat, lng) {
        if (this.isInDeepSh_t)
            this.One.getIds().then(success => {
                this.notif_id = success.userId
                this.eprov.UpdateLocation(lat, lng, success.userId)
            })
    }


}
