import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SystrasProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SystrasProvider {

  public baseUrl = "http://apiv54.systras.com.br/api/";

  constructor(public http: Http) {}

  setDados(name: string, 
    email: string, 
    firebase_id: string, 
    onesignal_id: string,
    company_code: string,
    password: string,
    banco_info: string,
    placa_carro: string,
    licenca_carro: string,
    modelo_carro: string,
    ano_carro: string,
    assentos_carro: string,
    tipo_carro: any,
    phone: any){
      let body = {
        name: name,
        email: email,
        firebase_id: firebase_id,
        onesignal_id: onesignal_id,
        company_code: company_code,
        password: password,
        banco_info: banco_info,
        placa_carro: placa_carro,
        licenca_carro: licenca_carro,
        modelo_carro: modelo_carro,
        ano_carro: ano_carro,
        assentos_carro: assentos_carro,
        tipo_carro: tipo_carro,
        phone: phone,
        new_systras: true
      };
      console.log(body);
      return this.http.post(`${this.baseUrl}novo_cadastro`, body);
    }



}
